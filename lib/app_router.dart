import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:restaurant_app/categories/domain/use_cases/category_use_cases.dart';
import 'package:restaurant_app/categories/presenters/bloc/categories_bloc.dart';
import 'package:restaurant_app/categories/presenters/screens/categories_screen.dart';
import 'package:restaurant_app/core/data/repositories/products_repository.dart';
import 'package:restaurant_app/core/presenters/screens/app_screen.dart';
import 'package:restaurant_app/products/domain/use_cases/product_use_cases.dart';
import 'package:restaurant_app/products/presenters/bloc/products_bloc.dart';
import 'package:restaurant_app/products/presenters/screens/products_screen.dart';
import 'package:restaurant_app/shopping_card/presenters/screens/shopping_card_screen.dart';

class AppRoutes {
  static const String categories = '/categories';
  static const String product = 'product/:id';
  static const String shoppingCard = '/shopping_card';

  static String productId(int id) => '/categories/product/$id';

  static Page _pageAnimation(Widget child) {
    return CustomTransitionPage(
      child: child,
      transitionsBuilder: (
        context,
        animation,
        secondaryAnimation,
        child,
      ) =>
          CupertinoPageTransition(
        primaryRouteAnimation: animation,
        secondaryRouteAnimation: Animation.fromValueListenable(
          animation,
          transformer: (value) {
            return (1 - value).abs() / 50;
          },
        ),
        linearTransition: false,
        child: ColoredBox(color: Colors.white, child: child),
      ),
    );
  }

  static Map<String, String> buildQuery(String title, String forPath) {
    return {
      'title': title,
      'path': forPath,
    };
  }

  static GoRouter appRouter = GoRouter(
    initialLocation: categories,
    routes: [
      ShellRoute(
        pageBuilder: (context, state, child) => _pageAnimation(
          AppScreen(
            location: state.location,
            canPop: '/'.allMatches(state.location).length > 1,
            title:
                state.fullPath?.contains(state.queryParameters['path'] ?? '') ??
                        false
                    ? state.queryParameters['title']
                    : null,
            child: child,
          ),
        ),
        routes: [
          GoRoute(
            name: categories,
            path: categories,
            pageBuilder: (context, state) => _pageAnimation(
              BlocProvider(
                create: (context) => CategoriesBloc(
                  CategoryUseCases(GetIt.I.get<ProductRepository>()),
                ),
                child: const CategoriesScreen(),
              ),
            ),
            routes: [
              GoRoute(
                name: product,
                path: product,
                pageBuilder: (context, state) {
                  final categoryId =
                      int.tryParse(state.pathParameters['id'] ?? '') ?? -1;
                  return _pageAnimation(
                    BlocProvider(
                      create: (context) => ProductsBloc(
                        ProductUseCases(GetIt.I.get<ProductRepository>()),
                        categoryId,
                      ),
                      child: ProductsScreen(categoryId: categoryId),
                    ),
                  );
                },
              )
            ],
          ),
          GoRoute(
            name: shoppingCard,
            path: shoppingCard,
            pageBuilder: (context, state) =>
                _pageAnimation(const ShoppingCardScreen()),
          ),
        ],
      ),
    ],
  );
}
