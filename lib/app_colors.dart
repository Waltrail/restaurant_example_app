import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color textColor = Color(0xFF2C2F39);
  static const Color lightTextColor = Color(0xFF747C8A);
  static const Color unactiveGreyColor = Color(0xFF9EA7B6);
  static const Color primaryColor = Color(0xFFFE5000);
  static const Color lightButtonColor = Color(0xFFF1F4F9);
}
