import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/app_router.dart';
import 'package:restaurant_app/core/presenters/widgets/appbar.dart';
import 'package:restaurant_app/core/presenters/widgets/menu_item.dart';
import 'package:restaurant_app/generated/l10n.dart';
import 'package:restaurant_app/shopping_card/presenters/bloc/shopping_card_bloc.dart';

class AppScreen extends StatelessWidget {
  const AppScreen({
    super.key,
    required this.child,
    required this.location,
    required this.canPop,
    this.title,
  });

  final Widget child;
  final String location;
  final bool canPop;
  final String? title;

  String titleBaseOnLocation(BuildContext context) {
    if (location.contains(AppRoutes.categories)) {
      return S.of(context).categories;
    }
    if (location.contains(AppRoutes.shoppingCard)) {
      return S.of(context).shopping_card;
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    final currentIndex = location == AppRoutes.shoppingCard ? 1 : 0;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: RestaurantAppBar(
        title: title ?? titleBaseOnLocation(context),
        backbutton: canPop,
      ),
      body: child,
      bottomNavigationBar: BlocBuilder<ShoppingCardBloc, ShoppingCardState>(
        builder: (context, state) {
          return DecoratedBox(
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: AppColors.lightButtonColor,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MenuItem(
                  title: S.of(context).food,
                  iconPath: 'assets/kitchen.svg',
                  isActive: currentIndex == 0,
                  onTap: () {
                    if (location != AppRoutes.categories) {
                      return GoRouter.of(context).go(AppRoutes.categories);
                    }
                  },
                ),
                const SizedBox(width: 32),
                MenuItem(
                  title: state.sum > 0
                      ? '${state.sum} ₽'
                      : S.of(context).shopping_card,
                  downAnimation:
                      state.sum < context.read<ShoppingCardBloc>().lastSum,
                  iconPath: state.sum > 0
                      ? 'assets/basket_happy.svg'
                      : 'assets/basket_sad.svg',
                  isActive: currentIndex == 1,
                  onTap: () {
                    if (location != AppRoutes.shoppingCard) {
                      return GoRouter.of(context).go(AppRoutes.shoppingCard);
                    }
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
