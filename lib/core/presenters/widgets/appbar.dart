import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:restaurant_app/app_colors.dart';

class RestaurantAppBar extends StatelessWidget implements PreferredSizeWidget {
  const RestaurantAppBar({
    super.key,
    required this.title,
    this.backbutton = false,
  });

  final String title;
  final bool backbutton;

  static const double kAppBarHeight = 56;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: kAppBarHeight,
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: Text(
        title,
        style: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w800,
          color: AppColors.textColor,
        ),
      ),
      leadingWidth: backbutton ? 48 : 0,
      leading: backbutton
          ? GestureDetector(
              onTap: () {
                if (context.canPop()) {
                  context.pop();
                }
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: SvgPicture.asset(
                  'assets/arrow_back.svg',
                  height: 24,
                  width: 24,
                  colorFilter: const ColorFilter.mode(
                    AppColors.textColor,
                    BlendMode.srcIn,
                  ),
                ),
              ),
            )
          : const SizedBox.shrink(),
    );
  }

  @override
  Size get preferredSize => const Size(double.infinity, kAppBarHeight);
}
