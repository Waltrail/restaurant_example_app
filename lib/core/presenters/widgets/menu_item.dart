import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:restaurant_app/app_colors.dart';

class MenuItem extends StatelessWidget {
  const MenuItem({
    super.key,
    required this.title,
    required this.iconPath,
    required this.isActive,
    required this.onTap,
    this.downAnimation = true,
  });

  final String title;
  final String iconPath;
  final bool isActive;
  final bool downAnimation;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        height: 56,
        width: 70,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 7,
                bottom: 5,
              ),
              child: SvgPicture.asset(
                iconPath,
                width: 24,
                height: 24,
                colorFilter: ColorFilter.mode(
                  isActive ? AppColors.textColor : AppColors.unactiveGreyColor,
                  BlendMode.srcATop,
                ),
              ),
            ),
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (child, animation) {
                return DualTransitionBuilder(
                  key: ValueKey<Key?>(child.key),
                  animation: animation,
                  forwardBuilder: (context, animation, child) {
                    return FadeTransition(
                      opacity: animation,
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: Offset(0, downAnimation ? 1 : -1),
                          end: Offset.zero,
                        ).animate(
                          CurvedAnimation(
                            parent: animation,
                            curve: Curves.linear,
                          ),
                        ),
                        child: child,
                      ),
                    );
                  },
                  reverseBuilder: (context, animation, child) {
                    return FadeTransition(
                      opacity: Animation.fromValueListenable(
                        animation,
                        transformer: (value) {
                          return (1 - value).abs();
                        },
                      ),
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: Offset.zero,
                          end: Offset(0, downAnimation ? -1 : 1),
                        ).animate(
                          CurvedAnimation(
                            parent: animation,
                            curve: Curves.linear,
                          ),
                        ),
                        child: child,
                      ),
                    );
                  },
                  child: child,
                );
              },
              child: Text(
                key: ValueKey(title),
                title,
                style: TextStyle(
                  fontSize: 12,
                  color: isActive
                      ? AppColors.textColor
                      : AppColors.unactiveGreyColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
