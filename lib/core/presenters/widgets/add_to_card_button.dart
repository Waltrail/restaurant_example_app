import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/generated/l10n.dart';
import 'package:restaurant_app/shopping_card/presenters/bloc/shopping_card_bloc.dart';

class AddToCardButton extends StatefulWidget {
  const AddToCardButton({
    super.key,
    required this.id,
    this.isBig = false,
    this.height = 36,
    this.width = 85,
    this.color = AppColors.primaryColor,
  });
  final int id;
  final bool isBig;
  final double height;
  final double width;
  final Color color;

  @override
  State<AddToCardButton> createState() => _AddToCardButtonState();
}

class _AddToCardButtonState extends State<AddToCardButton> {
  late Color adaptedColor;

  @override
  void initState() {
    super.initState();
    adaptedColor = widget.color.computeLuminance() >= 0.5
        ? AppColors.textColor
        : Colors.white;
  }

  @override
  void didUpdateWidget(covariant AddToCardButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    adaptedColor = widget.color.computeLuminance() >= 0.5
        ? AppColors.textColor
        : Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    final cardBloc = BlocProvider.of<ShoppingCardBloc>(context, listen: true);

    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: GestureDetector(
        onTap: cardBloc.countProductsInCard(widget.id) > 0
            ? null
            : () => cardBloc.add(
                  ShoppingCardEvent.addProduct(
                    productId: widget.id,
                  ),
                ),
        child: Container(
          height: widget.isBig ? 50 : widget.height,
          width: widget.isBig ? 120 : widget.width,
          decoration: BoxDecoration(
            color: widget.color,
            boxShadow: [
              BoxShadow(
                color: widget.color.withOpacity(0.16),
                blurRadius: 20,
                offset: const Offset(0, 4),
              )
            ],
          ),
          child: Center(
            child: cardBloc.countProductsInCard(widget.id) > 0
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: RawMaterialButton(
                          onPressed: () => cardBloc.add(
                            ShoppingCardEvent.removeProduct(
                              productId: widget.id,
                            ),
                          ),
                          child: SvgPicture.asset(
                            'assets/minus.svg',
                            width: 10,
                            height: 10,
                            colorFilter: ColorFilter.mode(
                              adaptedColor,
                              BlendMode.srcIn,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16,
                        child: Center(
                          child: Text(
                            '${cardBloc.countProductsInCard(widget.id)}',
                            style: TextStyle(
                              color: adaptedColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: RawMaterialButton(
                          onPressed: () => cardBloc.add(
                            ShoppingCardEvent.addProduct(
                              productId: widget.id,
                            ),
                          ),
                          child: SvgPicture.asset(
                            'assets/plus.svg',
                            width: 10,
                            height: 10,
                            colorFilter: ColorFilter.mode(
                              adaptedColor,
                              BlendMode.srcIn,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : widget.isBig
                    ? Text(
                        S.of(context).want_it,
                        style: TextStyle(
                          color: adaptedColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                        ),
                      )
                    : SvgPicture.asset(
                        'assets/plus.svg',
                        colorFilter: ColorFilter.mode(
                          adaptedColor,
                          BlendMode.srcIn,
                        ),
                      ),
          ),
        ),
      ),
    );
  }
}
