import 'package:restaurant_app/core/data/dto/category_dto.dart';
import 'package:restaurant_app/core/data/dto/product_dto.dart';

abstract interface class IProductsRepository {
  Future<List<CategoryDTO>> getAllCategories();
  Future<List<ProductDTO>> getAllProductsInCategory(int categoryId);
}
