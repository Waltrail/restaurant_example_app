import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:dio/dio.dart';
import 'package:restaurant_app/core/data/dto/category_dto.dart';
import 'package:restaurant_app/core/data/dto/product_dto.dart';
import 'package:restaurant_app/core/data/repositories/products_repository_interface.dart';

final class ProductRepository implements IProductsRepository {
  ProductRepository(this.dio);

  final Dio dio;

  List<CategoryDTO>? categories;

  @override
  Future<List<CategoryDTO>> getAllCategories() async {
    final response = await dio.get(
      '/moisey312/10b304f7b00ffd17535604f4b38ebe6a/raw/eeb0334ccf8e33fb4be7495a395ddc2ec22f3a75/test.json',
    );
    if (response.data != null) {
      final jsonData = jsonDecode(response.data);
      if (jsonData is! List) {
        return [];
      }

      return categories = jsonData
          .map((json) => CategoryDTO.fromJson(json as Map<String, Object?>))
          .toList();
    }

    return [];
  }

  @override
  Future<List<ProductDTO>> getAllProductsInCategory(int categoryId) async {
    if (categories?.isEmpty ?? false) {
      await getAllCategories();
    }

    return categories
            ?.firstWhereOrNull((category) => category.id == categoryId)
            ?.products ??
        [];
  }
}
