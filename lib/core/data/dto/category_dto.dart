import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:restaurant_app/core/data/dto/product_dto.dart';

part 'category_dto.freezed.dart';
part 'category_dto.g.dart';

@freezed
class CategoryDTO with _$CategoryDTO {
  const factory CategoryDTO({
    required int id,
    // ignore: non_constant_identifier_names
    required String category_name,
    // ignore: non_constant_identifier_names
    String? image_url,
    @Default([]) List<ProductDTO> products,
  }) = _CategoryDTO;

  factory CategoryDTO.fromJson(Map<String, Object?> json) =>
      _$CategoryDTOFromJson(json);
}
