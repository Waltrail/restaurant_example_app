import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_dto.freezed.dart';
part 'product_dto.g.dart';

@freezed
class ProductDTO with _$ProductDTO {
  const factory ProductDTO({
    required int id,
    required String name,
    // ignore: non_constant_identifier_names
    String? image_url,
    required int cost,
    String? sizes,
    int? categoryId,
    String? description,
  }) = _ProductDTO;

  factory ProductDTO.fromJson(Map<String, Object?> json) =>
      _$ProductDTOFromJson(json);
}
