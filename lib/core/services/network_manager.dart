import 'package:dio/dio.dart';

final class NetworkManager {
  factory NetworkManager() => _instance;

  NetworkManager._internal() {
    dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        connectTimeout: const Duration(seconds: 5),
        receiveTimeout: const Duration(seconds: 3),
      ),
    );
  }

  static final NetworkManager _instance = NetworkManager._internal();

  late final Dio dio;
  final String baseUrl = 'https://gist.githubusercontent.com';
}
