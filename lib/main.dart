import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:restaurant_app/app_router.dart';
import 'package:restaurant_app/categories/presenters/widgets/promo_action_notifier.dart';
import 'package:restaurant_app/core/data/repositories/products_repository.dart';
import 'package:restaurant_app/core/services/network_manager.dart';
import 'package:restaurant_app/generated/l10n.dart';
import 'package:restaurant_app/shopping_card/domain/use_cases/shopping_card_usecase.dart';
import 'package:restaurant_app/shopping_card/presenters/bloc/shopping_card_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  GetIt.I.registerSingleton<ProductRepository>(
    ProductRepository(NetworkManager().dio),
  );
  GetIt.I.registerSingleton<PromoActionNotifier>(PromoActionNotifier());
  Intl.defaultLocale = 'ru_RU';
  runApp(
    BlocProvider(
      create: (context) => ShoppingCardBloc(
        ShoppingCardUseCase(
          GetIt.I.get<ProductRepository>(),
        ),
      ),
      child: const MainApp(),
    ),
  );
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  void initState() {
    super.initState();
    GetIt.I.get<PromoActionNotifier>().startTimer();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return MaterialApp.router(
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      routerConfig: AppRoutes.appRouter,
      theme: ThemeData(
        textTheme: GoogleFonts.ralewayTextTheme(textTheme),
      ),
    );
  }
}
