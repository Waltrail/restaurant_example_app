import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PromoActionNotifier extends ValueNotifier<Duration?> {
  PromoActionNotifier() : super(null);

  Timer? _timer;

  DateTime? endTime;

  Future<void> startTimer() async {
    endTime ??= DateTime.fromMillisecondsSinceEpoch(
      (await SharedPreferences.getInstance()).getInt('endTime') ?? 0,
    );
    final difference = endTime?.difference(DateTime.now()) ?? Duration.zero;
    if (endTime == null || difference <= Duration.zero) {
      endTime = DateTime.now().add(const Duration(minutes: 10));
    } else {
      value = difference;
      notifyListeners();
    }
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      value = endTime?.difference(DateTime.now()) ?? Duration.zero;
      if (value! <= Duration.zero) {
        _timer?.cancel();
        value = Duration.zero;
        endTime = null;
        Future(
          () async => (await SharedPreferences.getInstance()).remove('endTime'),
        );
      }
      notifyListeners();
    });
    unawaited(
      (await SharedPreferences.getInstance())
          .setInt('endTime', endTime?.millisecondsSinceEpoch ?? 0),
    );
  }

  void stopTimer() {
    _timer?.cancel();
  }

  @override
  void dispose() {
    _timer?.cancel();
    _timer = null;
    super.dispose();
  }
}
