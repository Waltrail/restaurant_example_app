import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/app_router.dart';

class CategoryItemWidget extends StatefulWidget {
  const CategoryItemWidget({
    super.key,
    required this.title,
    required this.imageUrl,
    required this.id,
  });

  final String title;
  final String imageUrl;
  final int id;

  @override
  State<CategoryItemWidget> createState() => _CategoryItemWidgetState();
}

class _CategoryItemWidgetState extends State<CategoryItemWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animation;
  @override
  void initState() {
    super.initState();
    animation = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
      upperBound: 0.1,
    );
    animation.addListener(() {
      setState(() {
        animationScale = 1 - animation.value;
      });
    });
  }

  double animationScale = 1;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.goNamed(
          AppRoutes.product,
          pathParameters: {
            'id': widget.id.toString(),
          },
          queryParameters:
              AppRoutes.buildQuery(widget.title, AppRoutes.product),
        );
      },
      onTapDown: (_) => animation.forward(),
      onTapUp: (_) => animation.reverse(),
      onTapCancel: () => animation.reverse(),
      child: Transform.scale(
        scale: animationScale,
        child: Container(
          height: 175,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                color: Color(0x0a5666A7),
                blurRadius: 30,
                offset: Offset(0, 4),
              )
            ],
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(widget.imageUrl),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 15),
                  child: Text(
                    widget.title,
                    style: const TextStyle(
                      color: AppColors.textColor,
                      fontSize: 14,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
