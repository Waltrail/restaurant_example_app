import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/categories/presenters/widgets/promo_action_notifier.dart';
import 'package:restaurant_app/generated/l10n.dart';

class BannerWidget extends StatelessWidget {
  const BannerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Duration?>(
      valueListenable: GetIt.I.get<PromoActionNotifier>(),
      builder: (context, value, child) {
        if (value == null) {
          return const Center(
            child: CircularProgressIndicator(
              color: AppColors.primaryColor,
            ),
          );
        }

        if (value <= Duration.zero) {
          return GestureDetector(
            onTap: () => GetIt.I.get<PromoActionNotifier>().startTimer(),
            child: Text(
              S().restart_timer,
              style: const TextStyle(
                color: AppColors.textColor,
                fontSize: 16,
                fontWeight: FontWeight.w800,
              ),
            ),
          );
        }
        return Padding(
          padding: const EdgeInsets.only(
            bottom: 20,
          ),
          child: Stack(
            children: [
              Image.asset('assets/banner.png'),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  margin: const EdgeInsets.all(11),
                  padding: const EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Text(
                    '${value.inMinutes.remainder(60)}:'.padLeft(3, '0') +
                        '${value.inSeconds.remainder(60)}'.padLeft(2, '0'),
                    style: const TextStyle(
                      color: AppColors.textColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
