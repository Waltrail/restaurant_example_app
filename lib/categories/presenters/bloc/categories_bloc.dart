import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:restaurant_app/categories/domain/entities/category_entity.dart';
import 'package:restaurant_app/categories/domain/use_cases/category_use_cases.dart';

part 'categories_bloc.freezed.dart';

class CategoriesBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoriesBloc(this.useCases) : super(const CategoriesIdleState()) {
    on<LoadCategoriesEvent>(_load);
  }

  final CategoryUseCases useCases;

  Future<void> _load(
    LoadCategoriesEvent event,
    Emitter<CategoryState> emit,
  ) async {
    final (categories, error) = await useCases.getAllCategories();

    if (error != null && error.isNotEmpty) {
      return emit(CategoryState.error(message: error));
    }

    emit(CategoryState.loaded(categories: categories));
  }
}

@freezed
class CategoryEvent with _$CategoryEvent {
  const factory CategoryEvent.load() = LoadCategoriesEvent;
}

@freezed
class CategoryState with _$CategoryState {
  const factory CategoryState.idle() = CategoriesIdleState;
  const factory CategoryState.error({required String message}) =
      CategoriesErrorState;
  const factory CategoryState.loaded({
    required List<CategoryEntity> categories,
  }) = CategoriesLoadedState;
}
