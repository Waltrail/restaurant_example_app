// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'categories_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CategoryEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadCategoriesEvent value) load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadCategoriesEvent value)? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadCategoriesEvent value)? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryEventCopyWith<$Res> {
  factory $CategoryEventCopyWith(
          CategoryEvent value, $Res Function(CategoryEvent) then) =
      _$CategoryEventCopyWithImpl<$Res, CategoryEvent>;
}

/// @nodoc
class _$CategoryEventCopyWithImpl<$Res, $Val extends CategoryEvent>
    implements $CategoryEventCopyWith<$Res> {
  _$CategoryEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadCategoriesEventCopyWith<$Res> {
  factory _$$LoadCategoriesEventCopyWith(_$LoadCategoriesEvent value,
          $Res Function(_$LoadCategoriesEvent) then) =
      __$$LoadCategoriesEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadCategoriesEventCopyWithImpl<$Res>
    extends _$CategoryEventCopyWithImpl<$Res, _$LoadCategoriesEvent>
    implements _$$LoadCategoriesEventCopyWith<$Res> {
  __$$LoadCategoriesEventCopyWithImpl(
      _$LoadCategoriesEvent _value, $Res Function(_$LoadCategoriesEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadCategoriesEvent implements LoadCategoriesEvent {
  const _$LoadCategoriesEvent();

  @override
  String toString() {
    return 'CategoryEvent.load()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadCategoriesEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) {
    return load();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) {
    return load?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadCategoriesEvent value) load,
  }) {
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadCategoriesEvent value)? load,
  }) {
    return load?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadCategoriesEvent value)? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class LoadCategoriesEvent implements CategoryEvent {
  const factory LoadCategoriesEvent() = _$LoadCategoriesEvent;
}

/// @nodoc
mixin _$CategoryState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<CategoryEntity> categories) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<CategoryEntity> categories)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<CategoryEntity> categories)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CategoriesIdleState value) idle,
    required TResult Function(CategoriesErrorState value) error,
    required TResult Function(CategoriesLoadedState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CategoriesIdleState value)? idle,
    TResult? Function(CategoriesErrorState value)? error,
    TResult? Function(CategoriesLoadedState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CategoriesIdleState value)? idle,
    TResult Function(CategoriesErrorState value)? error,
    TResult Function(CategoriesLoadedState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryStateCopyWith<$Res> {
  factory $CategoryStateCopyWith(
          CategoryState value, $Res Function(CategoryState) then) =
      _$CategoryStateCopyWithImpl<$Res, CategoryState>;
}

/// @nodoc
class _$CategoryStateCopyWithImpl<$Res, $Val extends CategoryState>
    implements $CategoryStateCopyWith<$Res> {
  _$CategoryStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CategoriesIdleStateCopyWith<$Res> {
  factory _$$CategoriesIdleStateCopyWith(_$CategoriesIdleState value,
          $Res Function(_$CategoriesIdleState) then) =
      __$$CategoriesIdleStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CategoriesIdleStateCopyWithImpl<$Res>
    extends _$CategoryStateCopyWithImpl<$Res, _$CategoriesIdleState>
    implements _$$CategoriesIdleStateCopyWith<$Res> {
  __$$CategoriesIdleStateCopyWithImpl(
      _$CategoriesIdleState _value, $Res Function(_$CategoriesIdleState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CategoriesIdleState implements CategoriesIdleState {
  const _$CategoriesIdleState();

  @override
  String toString() {
    return 'CategoryState.idle()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CategoriesIdleState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<CategoryEntity> categories) loaded,
  }) {
    return idle();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<CategoryEntity> categories)? loaded,
  }) {
    return idle?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<CategoryEntity> categories)? loaded,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CategoriesIdleState value) idle,
    required TResult Function(CategoriesErrorState value) error,
    required TResult Function(CategoriesLoadedState value) loaded,
  }) {
    return idle(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CategoriesIdleState value)? idle,
    TResult? Function(CategoriesErrorState value)? error,
    TResult? Function(CategoriesLoadedState value)? loaded,
  }) {
    return idle?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CategoriesIdleState value)? idle,
    TResult Function(CategoriesErrorState value)? error,
    TResult Function(CategoriesLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class CategoriesIdleState implements CategoryState {
  const factory CategoriesIdleState() = _$CategoriesIdleState;
}

/// @nodoc
abstract class _$$CategoriesErrorStateCopyWith<$Res> {
  factory _$$CategoriesErrorStateCopyWith(_$CategoriesErrorState value,
          $Res Function(_$CategoriesErrorState) then) =
      __$$CategoriesErrorStateCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$CategoriesErrorStateCopyWithImpl<$Res>
    extends _$CategoryStateCopyWithImpl<$Res, _$CategoriesErrorState>
    implements _$$CategoriesErrorStateCopyWith<$Res> {
  __$$CategoriesErrorStateCopyWithImpl(_$CategoriesErrorState _value,
      $Res Function(_$CategoriesErrorState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$CategoriesErrorState(
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CategoriesErrorState implements CategoriesErrorState {
  const _$CategoriesErrorState({required this.message});

  @override
  final String message;

  @override
  String toString() {
    return 'CategoryState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CategoriesErrorState &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CategoriesErrorStateCopyWith<_$CategoriesErrorState> get copyWith =>
      __$$CategoriesErrorStateCopyWithImpl<_$CategoriesErrorState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<CategoryEntity> categories) loaded,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<CategoryEntity> categories)? loaded,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<CategoryEntity> categories)? loaded,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CategoriesIdleState value) idle,
    required TResult Function(CategoriesErrorState value) error,
    required TResult Function(CategoriesLoadedState value) loaded,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CategoriesIdleState value)? idle,
    TResult? Function(CategoriesErrorState value)? error,
    TResult? Function(CategoriesLoadedState value)? loaded,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CategoriesIdleState value)? idle,
    TResult Function(CategoriesErrorState value)? error,
    TResult Function(CategoriesLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class CategoriesErrorState implements CategoryState {
  const factory CategoriesErrorState({required final String message}) =
      _$CategoriesErrorState;

  String get message;
  @JsonKey(ignore: true)
  _$$CategoriesErrorStateCopyWith<_$CategoriesErrorState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CategoriesLoadedStateCopyWith<$Res> {
  factory _$$CategoriesLoadedStateCopyWith(_$CategoriesLoadedState value,
          $Res Function(_$CategoriesLoadedState) then) =
      __$$CategoriesLoadedStateCopyWithImpl<$Res>;
  @useResult
  $Res call({List<CategoryEntity> categories});
}

/// @nodoc
class __$$CategoriesLoadedStateCopyWithImpl<$Res>
    extends _$CategoryStateCopyWithImpl<$Res, _$CategoriesLoadedState>
    implements _$$CategoriesLoadedStateCopyWith<$Res> {
  __$$CategoriesLoadedStateCopyWithImpl(_$CategoriesLoadedState _value,
      $Res Function(_$CategoriesLoadedState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categories = null,
  }) {
    return _then(_$CategoriesLoadedState(
      categories: null == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<CategoryEntity>,
    ));
  }
}

/// @nodoc

class _$CategoriesLoadedState implements CategoriesLoadedState {
  const _$CategoriesLoadedState(
      {required final List<CategoryEntity> categories})
      : _categories = categories;

  final List<CategoryEntity> _categories;
  @override
  List<CategoryEntity> get categories {
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  @override
  String toString() {
    return 'CategoryState.loaded(categories: $categories)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CategoriesLoadedState &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_categories));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CategoriesLoadedStateCopyWith<_$CategoriesLoadedState> get copyWith =>
      __$$CategoriesLoadedStateCopyWithImpl<_$CategoriesLoadedState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<CategoryEntity> categories) loaded,
  }) {
    return loaded(categories);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<CategoryEntity> categories)? loaded,
  }) {
    return loaded?.call(categories);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<CategoryEntity> categories)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(categories);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CategoriesIdleState value) idle,
    required TResult Function(CategoriesErrorState value) error,
    required TResult Function(CategoriesLoadedState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CategoriesIdleState value)? idle,
    TResult? Function(CategoriesErrorState value)? error,
    TResult? Function(CategoriesLoadedState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CategoriesIdleState value)? idle,
    TResult Function(CategoriesErrorState value)? error,
    TResult Function(CategoriesLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class CategoriesLoadedState implements CategoryState {
  const factory CategoriesLoadedState(
          {required final List<CategoryEntity> categories}) =
      _$CategoriesLoadedState;

  List<CategoryEntity> get categories;
  @JsonKey(ignore: true)
  _$$CategoriesLoadedStateCopyWith<_$CategoriesLoadedState> get copyWith =>
      throw _privateConstructorUsedError;
}
