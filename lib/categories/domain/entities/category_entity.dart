final class CategoryEntity {
  CategoryEntity({required this.name, this.imageUrl = '', required this.id});
  final String name;
  final String imageUrl;
  final int id;
}
