import 'package:restaurant_app/categories/domain/entities/category_entity.dart';
import 'package:restaurant_app/core/data/dto/category_dto.dart';
import 'package:restaurant_app/core/data/repositories/products_repository_interface.dart';

final class CategoryUseCases {
  CategoryUseCases(this._productsRepository);

  final IProductsRepository _productsRepository;

  Future<(List<CategoryEntity>, String?)> getAllCategories() async {
    String? errorMessage;
    final result =
        await _productsRepository.getAllCategories().catchError((error) {
      errorMessage = error.toString();
      return <CategoryDTO>[];
    });

    return (
      result
          .map(
            (item) => CategoryEntity(
              name: item.category_name,
              imageUrl: item.image_url ?? '',
              id: item.id,
            ),
          )
          .toList(),
      errorMessage
    );
  }
}
