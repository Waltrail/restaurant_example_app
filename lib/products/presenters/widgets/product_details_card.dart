import 'package:flutter/material.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/core/presenters/widgets/add_to_card_button.dart';
import 'package:restaurant_app/products/domain/entities/product_entity.dart';

class ProductDetailsCard extends StatelessWidget {
  const ProductDetailsCard({super.key, required this.product});

  final ProductEntity product;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.network(
          product.imageUrl,
          width: double.infinity,
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 15,
            bottom: 24,
          ),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              product.name,
              style: const TextStyle(
                color: AppColors.textColor,
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              '${product.price} ₽',
              style: const TextStyle(
                color: AppColors.textColor,
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            AddToCardButton(
              isBig: true,
              id: product.id,
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 24),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              product.description,
              style: const TextStyle(
                color: AppColors.lightTextColor,
                fontSize: 12,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
