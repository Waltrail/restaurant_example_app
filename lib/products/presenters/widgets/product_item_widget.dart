import 'package:flutter/material.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/core/presenters/widgets/add_to_card_button.dart';
import 'package:restaurant_app/products/domain/entities/product_entity.dart';
import 'package:restaurant_app/products/presenters/widgets/product_details_card.dart';

class ProductItemWidget extends StatelessWidget {
  const ProductItemWidget({super.key, required this.product});

  final ProductEntity product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          constraints: const BoxConstraints(minHeight: 600),
          useRootNavigator: true,
          clipBehavior: Clip.hardEdge,
          useSafeArea: true,
          isScrollControlled: true,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(20),
            ),
          ),
          backgroundColor: Colors.white,
          context: context,
          builder: (context) {
            return Padding(
              padding: const EdgeInsets.all(20),
              child: ProductDetailsCard(
                product: product,
              ),
            );
          },
        );
      },
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0x085666A7),
              blurRadius: 30,
              offset: Offset(0, 6),
            )
          ],
        ),
        child: Column(
          children: [
            Image.network(
              product.imageUrl,
              height: 98,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 15,
              ),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  product.name,
                  style: const TextStyle(
                    color: AppColors.textColor,
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${product.price} ₽',
                      style: const TextStyle(
                        color: AppColors.textColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    AddToCardButton(
                      id: product.id,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
