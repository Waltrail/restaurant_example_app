import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:restaurant_app/products/domain/entities/product_entity.dart';
import 'package:restaurant_app/products/domain/use_cases/product_use_cases.dart';

part 'products_bloc.freezed.dart';

class ProductsBloc extends Bloc<ProductEvent, ProductState> {
  ProductsBloc(this.useCases, this.categoryId)
      : super(const ProductsIdleState()) {
    on<LoadProductsEvent>(_load);
  }

  final int categoryId;

  final ProductUseCases useCases;

  Future<void> _load(
    LoadProductsEvent event,
    Emitter<ProductState> emit,
  ) async {
    final (products, error) = await useCases.getProducts(categoryId);

    if (error != null && error.isNotEmpty) {
      return emit(ProductState.error(message: error));
    }

    emit(ProductState.loaded(products: products));
  }
}

@freezed
class ProductEvent with _$ProductEvent {
  const factory ProductEvent.load() = LoadProductsEvent;
}

@freezed
class ProductState with _$ProductState {
  const factory ProductState.idle() = ProductsIdleState;
  const factory ProductState.error({required String message}) =
      ProductsErrorState;
  const factory ProductState.loaded({
    required List<ProductEntity> products,
  }) = ProductsLoadedState;
}
