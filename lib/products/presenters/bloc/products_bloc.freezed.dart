// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'products_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProductEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadProductsEvent value) load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadProductsEvent value)? load,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadProductsEvent value)? load,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductEventCopyWith<$Res> {
  factory $ProductEventCopyWith(
          ProductEvent value, $Res Function(ProductEvent) then) =
      _$ProductEventCopyWithImpl<$Res, ProductEvent>;
}

/// @nodoc
class _$ProductEventCopyWithImpl<$Res, $Val extends ProductEvent>
    implements $ProductEventCopyWith<$Res> {
  _$ProductEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadProductsEventCopyWith<$Res> {
  factory _$$LoadProductsEventCopyWith(
          _$LoadProductsEvent value, $Res Function(_$LoadProductsEvent) then) =
      __$$LoadProductsEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadProductsEventCopyWithImpl<$Res>
    extends _$ProductEventCopyWithImpl<$Res, _$LoadProductsEvent>
    implements _$$LoadProductsEventCopyWith<$Res> {
  __$$LoadProductsEventCopyWithImpl(
      _$LoadProductsEvent _value, $Res Function(_$LoadProductsEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadProductsEvent implements LoadProductsEvent {
  const _$LoadProductsEvent();

  @override
  String toString() {
    return 'ProductEvent.load()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadProductsEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
  }) {
    return load();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
  }) {
    return load?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadProductsEvent value) load,
  }) {
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadProductsEvent value)? load,
  }) {
    return load?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadProductsEvent value)? load,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class LoadProductsEvent implements ProductEvent {
  const factory LoadProductsEvent() = _$LoadProductsEvent;
}

/// @nodoc
mixin _$ProductState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<ProductEntity> products) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<ProductEntity> products)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<ProductEntity> products)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductsIdleState value) idle,
    required TResult Function(ProductsErrorState value) error,
    required TResult Function(ProductsLoadedState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductsIdleState value)? idle,
    TResult? Function(ProductsErrorState value)? error,
    TResult? Function(ProductsLoadedState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductsIdleState value)? idle,
    TResult Function(ProductsErrorState value)? error,
    TResult Function(ProductsLoadedState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductStateCopyWith<$Res> {
  factory $ProductStateCopyWith(
          ProductState value, $Res Function(ProductState) then) =
      _$ProductStateCopyWithImpl<$Res, ProductState>;
}

/// @nodoc
class _$ProductStateCopyWithImpl<$Res, $Val extends ProductState>
    implements $ProductStateCopyWith<$Res> {
  _$ProductStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ProductsIdleStateCopyWith<$Res> {
  factory _$$ProductsIdleStateCopyWith(
          _$ProductsIdleState value, $Res Function(_$ProductsIdleState) then) =
      __$$ProductsIdleStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ProductsIdleStateCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductsIdleState>
    implements _$$ProductsIdleStateCopyWith<$Res> {
  __$$ProductsIdleStateCopyWithImpl(
      _$ProductsIdleState _value, $Res Function(_$ProductsIdleState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ProductsIdleState implements ProductsIdleState {
  const _$ProductsIdleState();

  @override
  String toString() {
    return 'ProductState.idle()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ProductsIdleState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<ProductEntity> products) loaded,
  }) {
    return idle();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<ProductEntity> products)? loaded,
  }) {
    return idle?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<ProductEntity> products)? loaded,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductsIdleState value) idle,
    required TResult Function(ProductsErrorState value) error,
    required TResult Function(ProductsLoadedState value) loaded,
  }) {
    return idle(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductsIdleState value)? idle,
    TResult? Function(ProductsErrorState value)? error,
    TResult? Function(ProductsLoadedState value)? loaded,
  }) {
    return idle?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductsIdleState value)? idle,
    TResult Function(ProductsErrorState value)? error,
    TResult Function(ProductsLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class ProductsIdleState implements ProductState {
  const factory ProductsIdleState() = _$ProductsIdleState;
}

/// @nodoc
abstract class _$$ProductsErrorStateCopyWith<$Res> {
  factory _$$ProductsErrorStateCopyWith(_$ProductsErrorState value,
          $Res Function(_$ProductsErrorState) then) =
      __$$ProductsErrorStateCopyWithImpl<$Res>;
  @useResult
  $Res call({String message});
}

/// @nodoc
class __$$ProductsErrorStateCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductsErrorState>
    implements _$$ProductsErrorStateCopyWith<$Res> {
  __$$ProductsErrorStateCopyWithImpl(
      _$ProductsErrorState _value, $Res Function(_$ProductsErrorState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = null,
  }) {
    return _then(_$ProductsErrorState(
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ProductsErrorState implements ProductsErrorState {
  const _$ProductsErrorState({required this.message});

  @override
  final String message;

  @override
  String toString() {
    return 'ProductState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductsErrorState &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductsErrorStateCopyWith<_$ProductsErrorState> get copyWith =>
      __$$ProductsErrorStateCopyWithImpl<_$ProductsErrorState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<ProductEntity> products) loaded,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<ProductEntity> products)? loaded,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<ProductEntity> products)? loaded,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductsIdleState value) idle,
    required TResult Function(ProductsErrorState value) error,
    required TResult Function(ProductsLoadedState value) loaded,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductsIdleState value)? idle,
    TResult? Function(ProductsErrorState value)? error,
    TResult? Function(ProductsLoadedState value)? loaded,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductsIdleState value)? idle,
    TResult Function(ProductsErrorState value)? error,
    TResult Function(ProductsLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ProductsErrorState implements ProductState {
  const factory ProductsErrorState({required final String message}) =
      _$ProductsErrorState;

  String get message;
  @JsonKey(ignore: true)
  _$$ProductsErrorStateCopyWith<_$ProductsErrorState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductsLoadedStateCopyWith<$Res> {
  factory _$$ProductsLoadedStateCopyWith(_$ProductsLoadedState value,
          $Res Function(_$ProductsLoadedState) then) =
      __$$ProductsLoadedStateCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ProductEntity> products});
}

/// @nodoc
class __$$ProductsLoadedStateCopyWithImpl<$Res>
    extends _$ProductStateCopyWithImpl<$Res, _$ProductsLoadedState>
    implements _$$ProductsLoadedStateCopyWith<$Res> {
  __$$ProductsLoadedStateCopyWithImpl(
      _$ProductsLoadedState _value, $Res Function(_$ProductsLoadedState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? products = null,
  }) {
    return _then(_$ProductsLoadedState(
      products: null == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<ProductEntity>,
    ));
  }
}

/// @nodoc

class _$ProductsLoadedState implements ProductsLoadedState {
  const _$ProductsLoadedState({required final List<ProductEntity> products})
      : _products = products;

  final List<ProductEntity> _products;
  @override
  List<ProductEntity> get products {
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  String toString() {
    return 'ProductState.loaded(products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductsLoadedState &&
            const DeepCollectionEquality().equals(other._products, _products));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_products));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductsLoadedStateCopyWith<_$ProductsLoadedState> get copyWith =>
      __$$ProductsLoadedStateCopyWithImpl<_$ProductsLoadedState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() idle,
    required TResult Function(String message) error,
    required TResult Function(List<ProductEntity> products) loaded,
  }) {
    return loaded(products);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? idle,
    TResult? Function(String message)? error,
    TResult? Function(List<ProductEntity> products)? loaded,
  }) {
    return loaded?.call(products);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? idle,
    TResult Function(String message)? error,
    TResult Function(List<ProductEntity> products)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(products);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ProductsIdleState value) idle,
    required TResult Function(ProductsErrorState value) error,
    required TResult Function(ProductsLoadedState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ProductsIdleState value)? idle,
    TResult? Function(ProductsErrorState value)? error,
    TResult? Function(ProductsLoadedState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ProductsIdleState value)? idle,
    TResult Function(ProductsErrorState value)? error,
    TResult Function(ProductsLoadedState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class ProductsLoadedState implements ProductState {
  const factory ProductsLoadedState(
      {required final List<ProductEntity> products}) = _$ProductsLoadedState;

  List<ProductEntity> get products;
  @JsonKey(ignore: true)
  _$$ProductsLoadedStateCopyWith<_$ProductsLoadedState> get copyWith =>
      throw _privateConstructorUsedError;
}
