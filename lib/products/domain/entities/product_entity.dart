class ProductEntity {
  ProductEntity({
    required this.id,
    required this.name,
    this.description = '',
    required this.price,
    this.imageUrl = '',
  });

  final int id;
  final String name;
  final String description;
  final int price;
  final String imageUrl;
}
