import 'package:restaurant_app/core/data/dto/product_dto.dart';
import 'package:restaurant_app/core/data/repositories/products_repository_interface.dart';
import 'package:restaurant_app/products/domain/entities/product_entity.dart';

final class ProductUseCases {
  ProductUseCases(this._productsRepository);

  final IProductsRepository _productsRepository;

  Future<(List<ProductEntity>, String?)> getProducts(int id) async {
    String? errorMessage;
    final result = await _productsRepository
        .getAllProductsInCategory(id)
        .catchError((error) {
      errorMessage = error.toString();
      return <ProductDTO>[];
    });

    return (
      result
          .map(
            (item) => ProductEntity(
              id: item.id,
              name: item.name,
              price: item.cost,
              imageUrl: item.image_url ?? '',
              description: item.description ?? '',
            ),
          )
          .toList(),
      errorMessage
    );
  }
}
