// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'shopping_card_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ShoppingCardEvent {
  int get productId => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int productId) addProduct,
    required TResult Function(int productId) removeProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int productId)? addProduct,
    TResult? Function(int productId)? removeProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int productId)? addProduct,
    TResult Function(int productId)? removeProduct,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductEvent value) addProduct,
    required TResult Function(RemoveProductEvent value) removeProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddProductEvent value)? addProduct,
    TResult? Function(RemoveProductEvent value)? removeProduct,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductEvent value)? addProduct,
    TResult Function(RemoveProductEvent value)? removeProduct,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ShoppingCardEventCopyWith<ShoppingCardEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShoppingCardEventCopyWith<$Res> {
  factory $ShoppingCardEventCopyWith(
          ShoppingCardEvent value, $Res Function(ShoppingCardEvent) then) =
      _$ShoppingCardEventCopyWithImpl<$Res, ShoppingCardEvent>;
  @useResult
  $Res call({int productId});
}

/// @nodoc
class _$ShoppingCardEventCopyWithImpl<$Res, $Val extends ShoppingCardEvent>
    implements $ShoppingCardEventCopyWith<$Res> {
  _$ShoppingCardEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
  }) {
    return _then(_value.copyWith(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AddProductEventCopyWith<$Res>
    implements $ShoppingCardEventCopyWith<$Res> {
  factory _$$AddProductEventCopyWith(
          _$AddProductEvent value, $Res Function(_$AddProductEvent) then) =
      __$$AddProductEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int productId});
}

/// @nodoc
class __$$AddProductEventCopyWithImpl<$Res>
    extends _$ShoppingCardEventCopyWithImpl<$Res, _$AddProductEvent>
    implements _$$AddProductEventCopyWith<$Res> {
  __$$AddProductEventCopyWithImpl(
      _$AddProductEvent _value, $Res Function(_$AddProductEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
  }) {
    return _then(_$AddProductEvent(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$AddProductEvent implements AddProductEvent {
  const _$AddProductEvent({required this.productId});

  @override
  final int productId;

  @override
  String toString() {
    return 'ShoppingCardEvent.addProduct(productId: $productId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddProductEvent &&
            (identical(other.productId, productId) ||
                other.productId == productId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, productId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddProductEventCopyWith<_$AddProductEvent> get copyWith =>
      __$$AddProductEventCopyWithImpl<_$AddProductEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int productId) addProduct,
    required TResult Function(int productId) removeProduct,
  }) {
    return addProduct(productId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int productId)? addProduct,
    TResult? Function(int productId)? removeProduct,
  }) {
    return addProduct?.call(productId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int productId)? addProduct,
    TResult Function(int productId)? removeProduct,
    required TResult orElse(),
  }) {
    if (addProduct != null) {
      return addProduct(productId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductEvent value) addProduct,
    required TResult Function(RemoveProductEvent value) removeProduct,
  }) {
    return addProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddProductEvent value)? addProduct,
    TResult? Function(RemoveProductEvent value)? removeProduct,
  }) {
    return addProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductEvent value)? addProduct,
    TResult Function(RemoveProductEvent value)? removeProduct,
    required TResult orElse(),
  }) {
    if (addProduct != null) {
      return addProduct(this);
    }
    return orElse();
  }
}

abstract class AddProductEvent implements ShoppingCardEvent {
  const factory AddProductEvent({required final int productId}) =
      _$AddProductEvent;

  @override
  int get productId;
  @override
  @JsonKey(ignore: true)
  _$$AddProductEventCopyWith<_$AddProductEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RemoveProductEventCopyWith<$Res>
    implements $ShoppingCardEventCopyWith<$Res> {
  factory _$$RemoveProductEventCopyWith(_$RemoveProductEvent value,
          $Res Function(_$RemoveProductEvent) then) =
      __$$RemoveProductEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int productId});
}

/// @nodoc
class __$$RemoveProductEventCopyWithImpl<$Res>
    extends _$ShoppingCardEventCopyWithImpl<$Res, _$RemoveProductEvent>
    implements _$$RemoveProductEventCopyWith<$Res> {
  __$$RemoveProductEventCopyWithImpl(
      _$RemoveProductEvent _value, $Res Function(_$RemoveProductEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productId = null,
  }) {
    return _then(_$RemoveProductEvent(
      productId: null == productId
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$RemoveProductEvent implements RemoveProductEvent {
  const _$RemoveProductEvent({required this.productId});

  @override
  final int productId;

  @override
  String toString() {
    return 'ShoppingCardEvent.removeProduct(productId: $productId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemoveProductEvent &&
            (identical(other.productId, productId) ||
                other.productId == productId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, productId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RemoveProductEventCopyWith<_$RemoveProductEvent> get copyWith =>
      __$$RemoveProductEventCopyWithImpl<_$RemoveProductEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int productId) addProduct,
    required TResult Function(int productId) removeProduct,
  }) {
    return removeProduct(productId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int productId)? addProduct,
    TResult? Function(int productId)? removeProduct,
  }) {
    return removeProduct?.call(productId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int productId)? addProduct,
    TResult Function(int productId)? removeProduct,
    required TResult orElse(),
  }) {
    if (removeProduct != null) {
      return removeProduct(productId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddProductEvent value) addProduct,
    required TResult Function(RemoveProductEvent value) removeProduct,
  }) {
    return removeProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddProductEvent value)? addProduct,
    TResult? Function(RemoveProductEvent value)? removeProduct,
  }) {
    return removeProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddProductEvent value)? addProduct,
    TResult Function(RemoveProductEvent value)? removeProduct,
    required TResult orElse(),
  }) {
    if (removeProduct != null) {
      return removeProduct(this);
    }
    return orElse();
  }
}

abstract class RemoveProductEvent implements ShoppingCardEvent {
  const factory RemoveProductEvent({required final int productId}) =
      _$RemoveProductEvent;

  @override
  int get productId;
  @override
  @JsonKey(ignore: true)
  _$$RemoveProductEventCopyWith<_$RemoveProductEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ShoppingCardState {
  List<ShoppingCardItemEntity> get items => throw _privateConstructorUsedError;
  int get sum => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ShoppingCardItemEntity> items, int sum) idle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ShoppingCardItemEntity> items, int sum)? idle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ShoppingCardItemEntity> items, int sum)? idle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ShoppingCardIdleState value) idle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ShoppingCardIdleState value)? idle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ShoppingCardIdleState value)? idle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ShoppingCardStateCopyWith<ShoppingCardState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShoppingCardStateCopyWith<$Res> {
  factory $ShoppingCardStateCopyWith(
          ShoppingCardState value, $Res Function(ShoppingCardState) then) =
      _$ShoppingCardStateCopyWithImpl<$Res, ShoppingCardState>;
  @useResult
  $Res call({List<ShoppingCardItemEntity> items, int sum});
}

/// @nodoc
class _$ShoppingCardStateCopyWithImpl<$Res, $Val extends ShoppingCardState>
    implements $ShoppingCardStateCopyWith<$Res> {
  _$ShoppingCardStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? items = null,
    Object? sum = null,
  }) {
    return _then(_value.copyWith(
      items: null == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<ShoppingCardItemEntity>,
      sum: null == sum
          ? _value.sum
          : sum // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ShoppingCardIdleStateCopyWith<$Res>
    implements $ShoppingCardStateCopyWith<$Res> {
  factory _$$ShoppingCardIdleStateCopyWith(_$ShoppingCardIdleState value,
          $Res Function(_$ShoppingCardIdleState) then) =
      __$$ShoppingCardIdleStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ShoppingCardItemEntity> items, int sum});
}

/// @nodoc
class __$$ShoppingCardIdleStateCopyWithImpl<$Res>
    extends _$ShoppingCardStateCopyWithImpl<$Res, _$ShoppingCardIdleState>
    implements _$$ShoppingCardIdleStateCopyWith<$Res> {
  __$$ShoppingCardIdleStateCopyWithImpl(_$ShoppingCardIdleState _value,
      $Res Function(_$ShoppingCardIdleState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? items = null,
    Object? sum = null,
  }) {
    return _then(_$ShoppingCardIdleState(
      items: null == items
          ? _value._items
          : items // ignore: cast_nullable_to_non_nullable
              as List<ShoppingCardItemEntity>,
      sum: null == sum
          ? _value.sum
          : sum // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ShoppingCardIdleState implements ShoppingCardIdleState {
  const _$ShoppingCardIdleState(
      {required final List<ShoppingCardItemEntity> items, required this.sum})
      : _items = items;

  final List<ShoppingCardItemEntity> _items;
  @override
  List<ShoppingCardItemEntity> get items {
    if (_items is EqualUnmodifiableListView) return _items;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_items);
  }

  @override
  final int sum;

  @override
  String toString() {
    return 'ShoppingCardState.idle(items: $items, sum: $sum)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShoppingCardIdleState &&
            const DeepCollectionEquality().equals(other._items, _items) &&
            (identical(other.sum, sum) || other.sum == sum));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_items), sum);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShoppingCardIdleStateCopyWith<_$ShoppingCardIdleState> get copyWith =>
      __$$ShoppingCardIdleStateCopyWithImpl<_$ShoppingCardIdleState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<ShoppingCardItemEntity> items, int sum) idle,
  }) {
    return idle(items, sum);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(List<ShoppingCardItemEntity> items, int sum)? idle,
  }) {
    return idle?.call(items, sum);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<ShoppingCardItemEntity> items, int sum)? idle,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle(items, sum);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ShoppingCardIdleState value) idle,
  }) {
    return idle(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ShoppingCardIdleState value)? idle,
  }) {
    return idle?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ShoppingCardIdleState value)? idle,
    required TResult orElse(),
  }) {
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class ShoppingCardIdleState implements ShoppingCardState {
  const factory ShoppingCardIdleState(
      {required final List<ShoppingCardItemEntity> items,
      required final int sum}) = _$ShoppingCardIdleState;

  @override
  List<ShoppingCardItemEntity> get items;
  @override
  int get sum;
  @override
  @JsonKey(ignore: true)
  _$$ShoppingCardIdleStateCopyWith<_$ShoppingCardIdleState> get copyWith =>
      throw _privateConstructorUsedError;
}
