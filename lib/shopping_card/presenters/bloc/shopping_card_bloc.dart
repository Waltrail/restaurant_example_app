import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:restaurant_app/shopping_card/domain/entities/shopping_card_item.dart';
import 'package:restaurant_app/shopping_card/domain/use_cases/shopping_card_usecase.dart';

part 'shopping_card_bloc.freezed.dart';

class ShoppingCardBloc extends Bloc<ShoppingCardEvent, ShoppingCardState> {
  ShoppingCardBloc(this._useCase)
      : super(const ShoppingCardIdleState(items: [], sum: 0)) {
    on<AddProductEvent>(_addProduct);
    on<RemoveProductEvent>(_removeProduct);
  }

  final ShoppingCardUseCase _useCase;

  int lastSum = 0;

  int countProductsInCard(int id) =>
      state.items.firstWhereOrNull((element) => element.id == id)?.quantity ??
      0;

  @override
  void onChange(Change<ShoppingCardState> change) {
    super.onChange(change);
    lastSum = change.currentState.sum;
  }

  Future<void> _addProduct(
    AddProductEvent event,
    Emitter<ShoppingCardState> emit,
  ) async {
    final items = await _useCase.addProduct(
      event.productId,
      state.items,
    );

    emit(
      ShoppingCardState.idle(
        items: items,
        sum: _useCase.calulateShoppingCard(items),
      ),
    );
  }

  void _removeProduct(
    RemoveProductEvent event,
    Emitter<ShoppingCardState> emit,
  ) {
    final items = _useCase.removeProduct(
      event.productId,
      state.items,
    );
    emit(
      ShoppingCardState.idle(
        items: items,
        sum: _useCase.calulateShoppingCard(items),
      ),
    );
  }
}

@freezed
class ShoppingCardEvent with _$ShoppingCardEvent {
  const factory ShoppingCardEvent.addProduct({required int productId}) =
      AddProductEvent;
  const factory ShoppingCardEvent.removeProduct({required int productId}) =
      RemoveProductEvent;
}

@freezed
class ShoppingCardState with _$ShoppingCardState {
  const factory ShoppingCardState.idle({
    required List<ShoppingCardItemEntity> items,
    required int sum,
  }) = ShoppingCardIdleState;
}
