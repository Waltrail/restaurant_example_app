import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/generated/l10n.dart';
import 'package:restaurant_app/shopping_card/presenters/bloc/shopping_card_bloc.dart';
import 'package:restaurant_app/shopping_card/presenters/widgets/shopping_card_item_widget.dart';

class ShoppingCardScreen extends StatelessWidget {
  const ShoppingCardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ShoppingCardBloc, ShoppingCardState>(
      builder: (context, state) {
        return state.when(
          idle: (items, sum) {
            if (sum == 0) {
              return Center(
                child: Text(
                  S.of(context).empty_card,
                  style: const TextStyle(
                    color: AppColors.textColor,
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              );
            }

            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: items
                      .map((item) => ShoppingCardItemWidget(item: item))
                      .toList(),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
