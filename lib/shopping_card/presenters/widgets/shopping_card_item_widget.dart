import 'package:flutter/material.dart';
import 'package:restaurant_app/app_colors.dart';
import 'package:restaurant_app/core/presenters/widgets/add_to_card_button.dart';
import 'package:restaurant_app/shopping_card/domain/entities/shopping_card_item.dart';

class ShoppingCardItemWidget extends StatelessWidget {
  const ShoppingCardItemWidget({super.key, required this.item});

  final ShoppingCardItemEntity item;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: double.infinity,
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: AppColors.lightButtonColor,
          ),
        ),
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 15, right: 15),
              child: AddToCardButton(
                id: item.id,
                color: AppColors.lightButtonColor,
                height: 40,
                width: 108,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                item.imageUrl,
                width: 40,
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.name,
                      style: const TextStyle(
                        color: AppColors.textColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      '${item.price} ₽',
                      style: const TextStyle(
                        color: AppColors.textColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    Text(
                      item.size,
                      style: const TextStyle(
                        color: AppColors.textColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
