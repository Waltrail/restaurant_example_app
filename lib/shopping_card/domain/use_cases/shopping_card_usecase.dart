import 'package:collection/collection.dart';
import 'package:restaurant_app/core/data/repositories/products_repository_interface.dart';
import 'package:restaurant_app/shopping_card/domain/entities/shopping_card_item.dart';

class ShoppingCardUseCase {
  ShoppingCardUseCase(this._productsRepository);

  final IProductsRepository _productsRepository;

  Future<List<ShoppingCardItemEntity>> addProduct(
    int id,
    List<ShoppingCardItemEntity> items,
  ) async {
    final products = List<ShoppingCardItemEntity>.from(items);
    final productInCard =
        products.firstWhereOrNull((product) => product.id == id);

    if (productInCard != null) {
      final index = products.indexOf(productInCard);
      products.replaceRange(index, index + 1, [
        ShoppingCardItemEntity(
          productInCard.id,
          productInCard.name,
          productInCard.price,
          productInCard.size,
          productInCard.quantity + 1,
          productInCard.imageUrl,
        ),
      ]);

      return products;
    }
    final categoriesWithProducts = await _productsRepository.getAllCategories();

    for (final category in categoriesWithProducts) {
      final product = category.products.firstWhereOrNull(
        (product) => product.id == id,
      );
      if (product != null) {
        products.add(
          ShoppingCardItemEntity(
            id,
            product.name,
            product.cost,
            product.sizes ?? '',
            1,
            product.image_url ?? '',
          ),
        );
        return products;
      }
    }

    return products;
  }

  List<ShoppingCardItemEntity> removeProduct(
    int id,
    List<ShoppingCardItemEntity> items,
  ) {
    final products = List<ShoppingCardItemEntity>.from(items);
    final productInCard =
        products.firstWhereOrNull((product) => product.id == id);

    if (productInCard != null) {
      final index = products.indexOf(productInCard);
      if (productInCard.quantity > 1) {
        products.replaceRange(index, index + 1, [
          ShoppingCardItemEntity(
            productInCard.id,
            productInCard.name,
            productInCard.price,
            productInCard.size,
            productInCard.quantity - 1,
            productInCard.imageUrl,
          ),
        ]);
      } else {
        products.remove(productInCard);
      }

      return products;
    }

    return products;
  }

  int calulateShoppingCard(List<ShoppingCardItemEntity> products) {
    var sum = 0;
    for (final item in products) {
      sum += item.price * item.quantity;
    }

    return sum;
  }
}
