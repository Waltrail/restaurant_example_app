import 'package:equatable/equatable.dart';

class ShoppingCardItemEntity extends Equatable {
  const ShoppingCardItemEntity(
    this.id,
    this.name,
    this.price,
    this.size,
    this.quantity,
    this.imageUrl,
  );

  final int id;
  final String name;
  final String imageUrl;
  final int price;
  final String size;
  final int quantity;

  @override
  List<Object?> get props => [id, name, price, size, quantity, imageUrl];
}
